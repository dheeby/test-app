const express = require('express');
const app = express();

app.get('/', (req, res) => {
  res.send('Hello World!!!');
});

const server = app.listen(9999, () => {
  console.log(`Express server running on PORT ${server.address().port}`);
});
